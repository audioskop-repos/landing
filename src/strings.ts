const strings: Record<"fr" | "en", Record<string, string>> = {
  fr: {},
  en: {
    [`Logiciel libre sous licence AGPL pour éditer et présenter des corpus audio.`]: `Free AGPL-licensed software to curate and present audio archives.`,
    [`Présentation de la version alpha (5mn).`]: `Video showcasing the Alpha version (5mn). English subtitles.`,
    [`Vidéo d'un parcours sur l'interface développé pour écran d'ordinateur. Montre l'état de notre travail au 25/11/24.`]: `Walkthrough of the desktop user interface as it was on 2024-11-25.`,
    [`Avec le soutien financier de la Direction régionale des affaires culturelles Occitanie`]: `With financial support from the Direction Régionale des Affaires Culturelles Occitanie`,
    [`Avec le soutien du Centre National des Arts Plastiques`]: `With support from the Centre National des Arts Plastiques`,
    [`Graphisme`]: `Graphic design`,
    [`Développement`]: `Development`,
    [`Vues de l'interface en cours de développement sur écran de téléphone.`]: `Mobile user interface views — development still in progress.`,
    [`Audioskop est aujourd'hui en version alpha (en cours de développement).`]: `Audioskop is as of today in Alpha version (work in progress)`,
    [`L'état en progrès est visible à`]: `Day-to-day progress is visible at `,
    [`Audioskop va passer par ces quelques phases jusqu'à sa sortie publique`]: `Audioskop will go through those phases before public release`,
    "Version alpha": "Alpha version",
    "Développement des fonctionnalités.": "Development of features",
    "Version bêta": "Beta version",
    "Fonctionnalités stabilisées, affinage et corrections.":
      "Stable features, refinement, and corrections",
    Prépublication: "Pre-publication",
    "Finitions, présentations restreintes, rédaction de la documentation.":
      "Finishing touches, limited presentations, documentation",
    "Sortie publique": "Public launch",
    "Ouverture publique du site, ouverture du code et de la documentation.":
      "Public launch of the site, opening of code and documentation",
    [`Tout au long du développement, le fonds d'archives de Pierre-Damien Huyghe est indexé et préparé pour être diffusé via Audioskop.`]: `Throughout development, Pierre-Damien Huyghe's archive fund is indexed and prepared to be broadcasted via Audioskop.`,
    [`Pour être informé de l'évolution du logiciel`]: `To stay informed about the software's evolution`,
    [`Lettre d'information`]: `Newsletter`,
    [`Une question ? Une remarque ? Des idées pour valoriser un corpus audio ?`]: `Any questions? Comments? Ideas on how to enhance an audio corpus?`,
    [`Écrivez-nous à l'adresse :`]: `Write to us at:`,
    [`Pour soutenir le projet`]: `To support the project`,
    [`Financement participatif`]: `Crowdfunding`,
  },
};

export const get_string = (lang: "fr" | "en", content: string): string => {
  return strings[lang || "fr"][content] || content;
};
